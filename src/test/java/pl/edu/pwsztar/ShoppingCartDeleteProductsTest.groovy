package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class ShoppingCartDeleteProductsTest extends Specification {

    static ShoppingCartOperation shoppingCart;

    def setupSpec() {
        shoppingCart = new ShoppingCart();
    }

    @Unroll
    def "should delete products in Cart"() {

        given:
        def deletedProducts = []

        when: "products are added and deleted from cart"
        shoppingCart.addProducts("paletka", 10, 1)
        shoppingCart.addProducts("baton", 3, 3)
        deletedProducts.add(shoppingCart.deleteProducts("baton", 1))
        deletedProducts.add(shoppingCart.deleteProducts("paletka", 2))
        then: "check if result is ok"
        deletedProducts == [true,false]
    }

}
