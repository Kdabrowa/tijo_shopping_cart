package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class ShoppingCartGetSumOfProductsTest extends Specification {

    static ShoppingCartOperation shoppingCart;

    def setupSpec() {
        shoppingCart = new ShoppingCart();
    }

    @Unroll
    def "should return quantity of wanted product in cart"() {

        when: "the account is created"
        shoppingCart.addProducts("paletka", 10, 7)
        shoppingCart.addProducts("paletka", 10, 1)
        shoppingCart.addProducts("baton", 3, 3)
        then: "check account number"
        shoppingCart.getSumProductsPrices()==89
    }

}
