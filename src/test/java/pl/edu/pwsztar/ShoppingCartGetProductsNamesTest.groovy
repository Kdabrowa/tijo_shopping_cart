package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class ShoppingCartGetProductsNamesTest extends Specification {

    static ShoppingCartOperation shoppingCart;

    def setupSpec() {
        shoppingCart = new ShoppingCart();
    }

    @Unroll
    def "should return quantity of wanted product in cart"() {

        given:
        def productNames = []

        when: "the account is created"
        shoppingCart.addProducts("paletka", 10, 7)
        shoppingCart.addProducts("baton", 10, 7)
        shoppingCart.addProducts("piłka", 10, 7)
        then: "check account number"
        shoppingCart.getProductsNames()==["paletka","baton","piłka"]
    }

}
