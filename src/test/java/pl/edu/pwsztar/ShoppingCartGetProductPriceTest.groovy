package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class ShoppingCartGetProductPriceTest extends Specification {

    static ShoppingCartOperation shoppingCart;

    def setupSpec() {
        shoppingCart = new ShoppingCart();
    }

    @Unroll
    def "should return quantity of wanted product in cart"() {

        given:
        def pricesProduct = []

        when: "the account is created"
        shoppingCart.addProducts("paletka", 10, 7)
        pricesProduct.add(shoppingCart.getProductPrice("paletka"))
        pricesProduct.add(shoppingCart.getProductPrice("baton"))
        then: "check account number"
        pricesProduct==[10,-1]
    }

}
