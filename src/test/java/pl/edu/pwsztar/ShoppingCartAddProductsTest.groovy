package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class ShoppingCartAddProductsTest extends Specification {

    static ShoppingCartOperation shoppingCart;

    def setupSpec() {
        shoppingCart = new ShoppingCart();
    }

    @Unroll
    def "should add products to cart"() {

        given:
        def addedProducts = []

        when: "products are added to cart"
        addedProducts.add(shoppingCart.addProducts("piłka", 20, 2))
        addedProducts.add(shoppingCart.addProducts("paletka", 10, 1))
        addedProducts.add(shoppingCart.addProducts("baton", 3, 3))
        addedProducts.add(shoppingCart.addProducts("baton", 3, 1))
        addedProducts.add(shoppingCart.addProducts("baton", 2, 1))
        then: "check if products were added to cart"
        addedProducts == [true,true,true,true,false]
    }
}
