package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class ShoppingCartGetQuantityTest extends Specification {

    static ShoppingCartOperation shoppingCart;

    def setupSpec() {
        shoppingCart = new ShoppingCart();
    }

    @Unroll
    def "should return quantity of wanted product in cart"() {

        given:
        def deletedProducts = []

        when: "the account is created"
        shoppingCart.addProducts("paletka", 10, 7)
        then: "check account number"
        shoppingCart.getQuantityOfProduct("paletka")==7
    }

}
