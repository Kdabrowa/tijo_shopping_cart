package pl.edu.pwsztar;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart implements ShoppingCartOperation {

    List<String> productsInCart = new ArrayList<String>();
    List<Integer> priceCart = new ArrayList<Integer>();
    List<Integer> quantityCart = new ArrayList<Integer>();
    public boolean addProducts(String productName, int price, int quantity) {
        int sumOfQuantity =0;
        for (int i = 0; i < quantityCart.size(); i++) {
            int result = quantityCart.get(i);
            sumOfQuantity += result;
        }
        if(sumOfQuantity<500) {
            if (productsInCart.contains(productName)) {
                int index = 0;
                for (int i = 0; i < productsInCart.size(); i++) {
                    if (productsInCart.get(i).equals(productName)) {
                        index = i;
                    }
                }
                if (priceCart.get(index) == price) {
                    int currentQuantity = quantityCart.get(index);
                    quantityCart.set(index, (currentQuantity + quantity));
                    return true;
                } else {
                    return false;
                }
            } else if (quantity > 0 && price > 0) {

                productsInCart.add(productName);
                priceCart.add(price);
                quantityCart.add(quantity);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public boolean deleteProducts(String productName, int quantity) {
        if(productsInCart.contains(productName)&&quantity>0) {
            int index=0;
            for(int i = 0;i<productsInCart.size();i++){
                if(productsInCart.get(i).equals(productName)){
                    index=i;
                }
            }
                int currentQuantity = quantityCart.get(index);
                if (currentQuantity>=quantity) {
                    quantityCart.set(index, currentQuantity - quantity);
                    return true;
                }
                else {
                    return false;
                }
        }
        return false;
    }

    public int getQuantityOfProduct(String productName) {
        int currentQuantity = 0;
        if(productsInCart.contains(productName)) {
            int index=0;
            for(int i = 0;i<productsInCart.size();i++){
                if(productsInCart.get(i).equals(productName)){
                    index=i;
                }
                currentQuantity = quantityCart.get(index);
            }
        }
        return currentQuantity;
    }

    public int getSumProductsPrices() {
        int sum =0;
        for (int i = 0; i < priceCart.size(); i++) {
            int result = priceCart.get(i) * quantityCart.get(i);
            sum += result;
        }
        return sum;
    }

    public int getProductPrice(String productName) {
        int price= 0;
        if(productsInCart.contains(productName)) {
            int index=0;
            for(int i = 0;i<productsInCart.size();i++){
                if(productsInCart.get(i).equals(productName)){
                    index=i;
                }
               price = priceCart.get(index);
            }
        } else {
            price=-1;
        }
        return price;
    }


    public List<String> getProductsNames() {
        return productsInCart;
    }
}
